import React from 'react';

import {Paper,Typography} from '@material-ui/core';
import ReactLoading from 'react-loading';

const VideoDetails = ({video}) =>{
    if(!video){ 
        return ( 
            <div style={{paddingTop:"30%" ,paddingLeft:"30%"}}>
                <ReactLoading type="spin" color="gray" height="10%" width="10%" />
            </div>
        )
    }
    const videoURL = "https://www.youtube.com/embed/"+video.id.videoId
    console.log(videoURL)
    return(
       <div>
           <Paper elevation={6} style={{height:"90%"}}>
            {/* iframe is used for the video  SRC*/}
                <iframe frameBorder="0" height="100%" width="100%" title="Video Player" src={videoURL} />
           </Paper>
           <Paper elevation={6} style={{padding:"15px"}}>
               {/* typography is like p */}
                <Typography variant="h6" style={{marginLeft:"-12px",fontWeight:"bold"}}>{video.snippet.title} - {video.snippet.channelTitle}</Typography>
                <Typography variant="subtitle1" style={{marginLeft:"-10px",fontWeight:"bold"}}>{video.snippet.channelTitle}</Typography>
                <Typography variant="subtitle2">{video.snippet.description}</Typography>
           </Paper>
       </div>
    )
}

export default VideoDetails;
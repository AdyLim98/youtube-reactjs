import React from 'react';
import {Paper,TextField} from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Grid from '@material-ui/core/Grid';
import SearchIcon from '@material-ui/icons/Search';

class SearchBar extends React.Component{
    state={
        searchTerms:'',        
    }
    
    handleChange = (e) =>{
        this.setState({
            searchTerms: e.target.value
        })
    }

    handleSubmit = (event) => {
        const {searchTerms} = this.state
        // 2.get the onFormSubmit from app.js
        const {onFormSubmit} = this.props
        // 3.pass the parameter searchTerms from this page back to app.js
        // 4.总之你imagine app.js 的handleSubmit 将拉来这里进行功能
        onFormSubmit(searchTerms)
        event.preventDefault()
    }

    render(){
        return(
            //elevation is like box-shadow
            <div>
            <Paper elevation={6} style={{padding:'2px',margin:"20px",width:"50%",marginLeft:"25%"}}>
                <form onSubmit={this.handleSubmit}>
                    <Grid container alignItems="flex-end" spacing={2}>
                        <Grid item>
                            <SearchIcon />
                        </Grid>
                        <Grid item>
                            <TextField style={{width:"320%"}} label="Search..." onChange={this.handleChange}/>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            {/* <p>{this.state.searchTerms}</p> */}
            </div>
            
        )
    }
}

export default SearchBar;
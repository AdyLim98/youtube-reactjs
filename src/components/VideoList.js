import React from 'react';
import {Grid} from '@material-ui/core';
import ReactLoading from 'react-loading';

import VideoItem from './VideoItem';
const VideoList = ({list,onVideoSelect}) =>{
    const listVideo = list.map((video,id)=> <VideoItem onVideoSelect={onVideoSelect} key={id} video={video}/>)
    
    if(!list){ 
        return( 
            <div>
                <ReactLoading type="spin" color="gray" height="10%" width="10%" />
            </div>
        )
    }
    return (
        <Grid container spacing={1}>{listVideo}</Grid>
    )
}

export default VideoList;
import React from 'react';

import {Grid} from '@material-ui/core';
import {SearchBar,VideoList,VideoDetails} from './components/index';
// import SearchBar from './components/SearchBar';
// import VideoDetails from './components/VideoDetails';
// import VideoList from './components/VideoList';
import youtube from './api/youtubeAPI';

class App extends React.Component{
    state ={
        video:[],
        selectedVideo:null,
    }

    componentDidMount(){
        this.handleSubmit("Tom and Jerry")
    }

    handleSubmit = async (searchTerm) =>{
        //which means the iniside youtube.js api url + /search ,q means query which define by youtube their site 
        const response = await youtube.get('search',{
            params:{
                //snippet is for video
                part:'snippet',
                //5 video 
                maxResults:5,
                key:process.env.REACT_APP_API_KEY,
                q:searchTerm
            }
        })
        this.setState({video:response.data.items,selectedVideo:response.data.items[0]})
        console.log(response.data)
    }

    onVideoSelect = (video) => {
        this.setState({
            selectedVideo:video
        })
    }

    render(){
        const {selectedVideo,video} = this.state;
        console.log("A:",selectedVideo)
        console.log("B:",video)
        return(
            <Grid justify="center"  container spacing={10}>
                <Grid item xs={12}>
                    <Grid container spacing={10}>
                        <Grid item xs={12}>
                            {/* 1.onFormSubmit pass  the handleSubmit  */}
                            <SearchBar onFormSubmit={this.handleSubmit} />
                        </Grid>
                        <Grid item xs={8}>
                            <VideoDetails video={selectedVideo}/>
                        </Grid>
                        <Grid item xs={4}>
                            <VideoList list={video} onVideoSelect={this.onVideoSelect}/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default App;